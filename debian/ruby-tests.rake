require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  #TODO: need to enable the other tests
  spec.pattern = './spec/database_cleaner/redis/*_spec.rb'
end
